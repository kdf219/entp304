package entp.entp304;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Kelli on 11/29/17.
 */

public class farmerRequest {
    String jobTitle;
    int farmerId;
    double offeredWage;
    int numWorkers;
    int numHours;
    int requestId;
    String farmName;

    @Override
    public String toString() {
        return "farmerRequest{" +
                "jobTitle='" + jobTitle + '\'' +
                ", farmerId=" + farmerId +
                ", offeredWage=" + offeredWage +
                ", numWorkers=" + numWorkers +
                ", numHours=" + numHours +
                ", requestId=" + requestId +
                ", farmName='" + farmName + '\'' +
                '}';
    }

    public farmerRequest(String jobTitle, int farmerId, double offeredWage, int numWorkers, int numHours, int requestId, String farmName) {
        this.jobTitle = jobTitle;
        this.farmerId = farmerId;
        this.offeredWage = offeredWage;
        this.numWorkers = numWorkers;
        this.numHours = numHours;
        this.requestId = requestId;
        this.farmName = farmName;
    }

}
