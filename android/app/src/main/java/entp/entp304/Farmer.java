package entp.entp304;

/**
 * Created by Kelli on 11/29/17.
 */

public class Farmer {

//     "uId": 3,
//             "username": "asdfa",
//             "farmName": "dafsd",
//             "contactName": "efasdf",
//             "email": "ewfasdf",
//             "phoneNumber": "fad"

    int farmerId;
    String username;
    String farmName;
    String contactName;
    String email;
    String phoneNumber;

    public Farmer(int farmerId, String username, String farmName, String contactName, String email, String phoneNumber) {
        this.farmerId = farmerId;
        this.username = username;
        this.farmName = farmName;
        this.contactName = contactName;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    public Farmer(){
        this.farmerId = 0;
        this.username = "";
        this.farmName = "";
        this.contactName = "";
        this.email = "";
        this.phoneNumber = "0000000000";
    }
}
