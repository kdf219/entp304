package entp.entp304;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Kelli on 11/29/17.
 */

public class JobInfoActivity extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.job_info);

        Intent info = getIntent();
//        String farmerId = info.getStringExtra("farmerId");
//        String requestId = info.getStringExtra("requestId");

//        MySingleton.getInstance(this).addToRequestQueue(getFarmerResponse(farmerId));
//        MySingleton.getInstance(this).addToRequestQueue(getFarmerRequestResponse(requestId));

        Log.d("entp304", "phone: " + info.getStringExtra("phoneNumber"));
        ((TextView) findViewById(R.id.jobTitle)).setText(info.getStringExtra("jobTitle"));
        ((TextView) findViewById(R.id.wage)).setText("$" + info.getStringExtra("wage") + "/hour");
        ((TextView) findViewById(R.id.numHours)).setText(info.getStringExtra("numHours"));
        ((TextView) findViewById(R.id.numWorkers)).setText(info.getStringExtra("numWorkers"));
        ((TextView) findViewById(R.id.farmNameInfo)).setText(info.getStringExtra("farmName"));
        ((TextView) findViewById(R.id.email)).setText(info.getStringExtra("email"));
        String phone = info.getStringExtra("phoneNumber");

//        phone = "(" + phone;
//        phone = phone.substring(0,4) + ") " + phone.substring(4);
//        phone = phone.substring(0,9) + " - " + phone.substring(9);
        ((TextView) findViewById(R.id.phoneNumber)).setText(phone);

        Button backButton = (Button) findViewById(R.id.backButton);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent goBack = new Intent(getApplicationContext(), JobsActivity.class);
                startActivityForResult(goBack, 123);
            }
        });

        setResult(Activity.RESULT_OK);
    }


//    Farmer farmer;
//    entp.entp304.farmerRequest farmerRequest;


//    public void onBindViewHolder(JobInfoActivity.ViewHolder holder, int position) {
//        Intent info = getIntent();
//        String farmerId = info.getStringExtra("farmerId");
//        String requestId = info.getStringExtra("requestId");
//        MySingleton.getInstance(this).addToRequestQueue(getFarmerResponse(farmerId));
//        MySingleton.getInstance(this).addToRequestQueue(getFarmerRequestResponse(requestId));
//
//        holder.jobTitle.setText(farmerRequest.jobTitle);
//        holder.offeredWage.setText(farmerRequest.offeredWage + "");
//        holder.numHours.setText(farmerRequest.numHours + "");
//        holder.numWorkers.setText(farmerRequest.numWorkers + "");
//        holder.farmName.setText(farmer.farmName);
//        holder.email.setText(farmer.email);
//        holder.phone.setText(farmer.phoneNumber);
//    }
//
//    protected StringRequest getFarmerResponse(String farmerId) {
//        return (new StringRequest(Request.Method.GET, Variables.url + "farmers/" + farmerId,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        Log.d("entp304", "VOLLEY: " + response);
//                        getFarmer(response);
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.e("entp304", "That Get didn't work!");
//            }
//        }));
//    }
//
//    void getFarmer(String response) {
//        try {
//            JSONObject jsonObj = new JSONObject(response);
//            JSONArray json = jsonObj.getJSONArray("mData");
//            for (int i = 0; i < json.length(); ++i) {
//                Log.d("entp304", "OBJ " + (i + 1) + ": " + json.getJSONObject(i).toString());
//                String username = json.getJSONObject(i).getString("username");
//                int farmerId = json.getJSONObject(i).getInt("uId");
//                String farmName = json.getJSONObject(i).getString("farmName");
//                String contactName = json.getJSONObject(i).getString("contactName");
//                String email = json.getJSONObject(i).getString("email");
//                String phoneNumber = json.getJSONObject(i).getString("phoneNumber");
//                Log.d("entp304", "HAPPENED 2: " + farmerId);
//                farmer = new Farmer(farmerId, username, farmName, contactName, email, phoneNumber);
//            }
//
//        } catch (final JSONException e) {
//            e.printStackTrace();
//            return;
//        }
//    }
//
//    protected StringRequest getFarmerRequestResponse(String requestId) {
//        return (new StringRequest(Request.Method.GET, Variables.url + "requests/" + requestId,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        Log.d("entp304", "VOLLEY: " + response);
//                        getFarmerRequest(response);
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.e("entp304", "That Get didn't work!");
//            }
//        }));
//    }
//
//    void getFarmerRequest(String response) {
//        try {
//            JSONObject jsonObj = new JSONObject(response);
//            JSONArray json = jsonObj.getJSONArray("mData");
//            for (int i = 0; i < json.length(); ++i) {
//                Log.d("entp304", "OBJ " + (i + 1) + ": " + json.getJSONObject(i).toString());
//                int requestId = json.getJSONObject(i).getInt("requestId");
//                int farmerId = json.getJSONObject(i).getInt("farmerId");
//                int numWorkers = json.getJSONObject(i).getInt("numWorkers");
//                int numHoursPerWorker = json.getJSONObject(i).getInt("numHoursPerWorker");
//                double hourlyRate = json.getJSONObject(i).getInt("hourlyRate");
//                String jobTitle = json.getJSONObject(i).getString("jobTitle");
//
//                Log.d("entp304", "HAPPENED: " + requestId);
//
//                farmerRequest = new farmerRequest(jobTitle, farmerId, hourlyRate, numWorkers, numHoursPerWorker, requestId, farmer.farmName);
//            }
//
//        } catch (final JSONException e) {
//            e.printStackTrace();
//            return;
//        }
//    }


//    class ViewHolder extends RecyclerView.ViewHolder {
//        TextView jobTitle;
//        TextView farmName;
//        TextView offeredWage;
//        TextView numHours;
//        TextView numWorkers;
//        TextView email;
//        TextView phone;
//
//        ViewHolder(View itemView) {
//            super(itemView);
//            this.jobTitle = (TextView) itemView.findViewById(R.id.jobTitle);
//            this.farmName = (TextView) itemView.findViewById(R.id.farmName);
//            this.offeredWage = (TextView) itemView.findViewById(R.id.wage);
//            this.numHours = (TextView) itemView.findViewById(R.id.numHours);
//            this.numWorkers = (TextView) itemView.findViewById(R.id.numWorkers);
//            this.email = (TextView) itemView.findViewById(R.id.email);
//            this.phone = (TextView) itemView.findViewById(R.id.phoneNumber);
//        }
//    }

    public void onBackPressed() {
        //do whatever you want the 'Back' button to do
        //as an example the 'Back' button is set to start a new Activity named 'NewActivity'
        Intent goBack = new Intent(getApplicationContext(), JobsActivity.class);
        startActivityForResult(goBack, 123);
    }

}