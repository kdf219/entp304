package entp.entp304;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static java.lang.Thread.sleep;

/**
 * Created by Kelli on 11/29/17.
 */

public class RequestListAdapter extends RecyclerView.Adapter<RequestListAdapter.ViewHolder> {
    private LayoutInflater mLayoutInflater;
    private ArrayList<farmerRequest> requests;
    private HashMap<Integer, Farmer> farmerHashMap;

    RequestListAdapter(Context context, ArrayList<farmerRequest> data, HashMap<Integer, Farmer> fData) {
        requests = data;
        farmerHashMap = fData;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d("entp304", "ON CREATE VIEW");
        View view = mLayoutInflater.inflate(R.layout.list_item, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Log.d("entp304", "ON BIND VIEW");
        final farmerRequest r = requests.get(position);

//        Request temp = getFarmerResponse(r.farmerId + "");
//        MySingleton.getInstance(MySingleton.getContext().getApplicationContext()).addToRequestQueue(temp);

//        Log.d("entp304", "calling getFarmerResponse " + r.farmerId);
//        getFarmerResponse(r.farmerId + "");
//        Log.d("entp304", "called getFarmerResponse " + r.farmerId);

        holder.jobTitle.setText(r.jobTitle);

        holder.farmName.setText(farmerHashMap.get(r.farmerId).farmName);

        holder.offeredWage.setText(r.offeredWage + "");

        final View.OnClickListener listener = new View.OnClickListener() { //to view comments
            @Override
            public void onClick(View view) {
                Log.d("entp304", "CLICKED");
                Log.d("entp304", "farmer id: " + r.farmerId);

                Intent clickedMsg = new Intent(MySingleton.getContext(), JobInfoActivity.class);
                //send info to clicked action

                clickedMsg.putExtra("jobTitle", "" + r.jobTitle);
                clickedMsg.putExtra("wage", "" + r.offeredWage);
                clickedMsg.putExtra("numHours", "" + r.numHours);
                clickedMsg.putExtra("numWorkers", "" + r.numWorkers);

                Log.d("entp304", "hours, workers: " + r.numHours + ", " + r.numWorkers);

                getFarmerResponse(r.farmerId + "");

                Log.d("entp304", "FARM NAME: " + farmerHashMap.get(r.farmerId).farmName);
                clickedMsg.putExtra("farmName", "" + farmerHashMap.get(r.farmerId).farmName);
                clickedMsg.putExtra("email", "" + farmerHashMap.get(r.farmerId).email);
                clickedMsg.putExtra("phoneNumber", "" + farmerHashMap.get(r.farmerId).phoneNumber);

//                ((TextView) findViewById(R.id.jobTitle)).setText(farmerRequest.jobTitle);
//                ((TextView) findViewById(R.id.wage)).setText(farmerRequest.offeredWage + "");
//                ((TextView) findViewById(R.id.numHours)).setText(farmerRequest.numHours + "");
//                ((TextView) findViewById(R.id.numWorkers)).setText(farmerRequest.numWorkers + "");
//                ((TextView) findViewById(R.id.farmName)).setText(farmer.farmName);
//                ((TextView) findViewById(R.id.email)).setText(farmer.email);
//                ((TextView) findViewById(R.id.phoneNumber)).setText(farmer.phoneNumber);

                MySingleton.getContext().startActivity(clickedMsg);
            }
        };

        holder.jobTitle.setOnClickListener(listener);
        holder.farmName.setOnClickListener(listener);
        holder.offeredWage.setOnClickListener(listener);
    }

    @Override
    public int getItemCount() {
        return requests.size();
    }

    Farmer farmer = new Farmer();
//    entp.entp304.farmerRequest farmerRequest;


//    public void onBindViewHolder(JobInfoActivity.ViewHolder holder, int position) {
//        Intent info = getIntent();
//        String farmerId = info.getStringExtra("farmerId");
//        String requestId = info.getStringExtra("requestId");
//        MySingleton.getInstance(this).addToRequestQueue(getFarmerResponse(farmerId));
//        MySingleton.getInstance(this).addToRequestQueue(getFarmerRequestResponse(requestId));
//
//        holder.jobTitle.setText(farmerRequest.jobTitle);
//        holder.offeredWage.setText(farmerRequest.offeredWage + "");
//        holder.numHours.setText(farmerRequest.numHours + "");
//        holder.numWorkers.setText(farmerRequest.numWorkers + "");
//        holder.farmName.setText(farmer.farmName);
//        holder.email.setText(farmer.email);
//        holder.phone.setText(farmer.phoneNumber);
//    }

    protected void getFarmerResponse(String farmerId) {
        MySingleton.getInstance(MySingleton.getContext().getApplicationContext()).addToRequestQueue(
                new StringRequest(Request.Method.GET, Variables.url + "farmers/" + farmerId,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d("entp304", "VOLLEY: " + response);
                                getFarmer(response);
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("entp304", "That Get didn't work!");
                    }
                })
        );
    }

    private Farmer getFarmer(String response) {
        try {
            JSONObject jsonObj = new JSONObject(response);
            JSONObject json = jsonObj.getJSONObject("mData");
            farmer.username = json.getString("username");
            farmer.farmerId = json.getInt("uId");
            farmer.farmName = json.getString("farmName");
            farmer.contactName = json.getString("contactName");
            farmer.email = json.getString("email");
            farmer.phoneNumber = json.getString("phoneNumber");

            Log.d("entp304", "HAPPENED: " + farmer.farmName + ", " + farmer.farmerId);
//            farmer = new Farmer(farmerId, username, farmName, contactName, email, phoneNumber);
            return farmer;
        } catch (final JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

//    protected StringRequest getFarmerRequestResponse(String requestId) {
//        return (new StringRequest(Request.Method.GET, Variables.url + "requests/" + requestId,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        Log.d("entp304", "VOLLEY: " + response);
//                        getFarmerRequest(response);
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.e("entp304", "That Get didn't work!");
//            }
//        }));
//    }
//
//    void getFarmerRequest(String response) {
//        try {
//            JSONObject jsonObj = new JSONObject(response);
//            JSONArray json = jsonObj.getJSONArray("mData");
//            for (int i = 0; i < json.length(); ++i) {
//                Log.d("entp304", "OBJ " + (i + 1) + ": " + json.getJSONObject(i).toString());
//                int requestId = json.getJSONObject(i).getInt("requestId");
//                int farmerId = json.getJSONObject(i).getInt("farmerId");
//                int numWorkers = json.getJSONObject(i).getInt("numWorkers");
//                int numHoursPerWorker = json.getJSONObject(i).getInt("numHoursPerWorker");
//                double hourlyRate = json.getJSONObject(i).getInt("hourlyRate");
//                String jobTitle = json.getJSONObject(i).getString("jobTitle");
//
//                Log.d("entp304", "HAPPENED: " + requestId);
//
//                farmerRequest = new farmerRequest(jobTitle, farmerId, hourlyRate, numWorkers, numHoursPerWorker, requestId, farmer.farmName);
//            }
//
//        } catch (final JSONException e) {
//            e.printStackTrace();
//            return;
//        }
//    }



    class ViewHolder extends RecyclerView.ViewHolder {
        TextView jobTitle;
        TextView farmName;
        TextView offeredWage;

        ViewHolder(View itemView) {
            super(itemView);
            this.jobTitle = (TextView) itemView.findViewById(R.id.jobTitle);
            this.farmName = (TextView) itemView.findViewById(R.id.farmName);
            this.offeredWage = (TextView) itemView.findViewById(R.id.wage);
        }
    }
}