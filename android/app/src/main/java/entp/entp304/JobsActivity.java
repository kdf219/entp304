package entp.entp304;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class JobsActivity extends AppCompatActivity {
    static RecyclerView rv;
    static RequestListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_jobs);

        rv = (RecyclerView) findViewById(R.id.request_list);
        adapter = new RequestListAdapter(this, mData, fData);
        MySingleton.getInstance(this).addToRequestQueue(getResponse());
        MySingleton.getInstance(this).addToRequestQueue(getFarmerResponse());

        Button refreshButton = (Button) findViewById(R.id.refreshList);

        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent refresh = new Intent(getApplicationContext(), JobsActivity.class);
                startActivityForResult(refresh, 123);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_jobs, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    ArrayList<farmerRequest> mData = new ArrayList<>();
    HashMap<Integer, Farmer> fData = new HashMap<>();
    Farmer helperFarmer = new Farmer(0, "", "nameFarm", "", "", "");

    protected StringRequest getFarmerResponse() {
        return (new StringRequest(Request.Method.GET, Variables.url + "farmers",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("entp304", "VOLLEY 2: " + response);
                        getFarmer(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("entp304", "That Get didn't work!");
            }
        }));
    }

    void getFarmer(String response) {
        try {
            JSONObject json1 = new JSONObject(response);
//            JSONObject json = json1.getJSONObject("mData");
            JSONArray json = json1.getJSONArray("mData");
            for (int i = 0; i < json.length(); ++i) {
                Log.d("entp304", "RESPONSE: " + response);
                String username = json.getJSONObject(i).getString("username");
                int farmerId = json.getJSONObject(i).getInt("uId");
                String farmName = json.getJSONObject(i).getString("farmName");
                String contactName = json.getJSONObject(i).getString("contactName");
                String email = json.getJSONObject(i).getString("email");
                String phoneNumber = json.getJSONObject(i).getString("phoneNumber");

                helperFarmer = new Farmer(farmerId, username, farmName, contactName, email, phoneNumber);

                Log.d("entp304", "putting in hashmap");
                fData.put(farmerId, helperFarmer);
            }

        } catch (final JSONException e) {
            e.printStackTrace();
            return;
        }
    }


    private void populateList(String response) {
        try {
            JSONObject jsonObj = new JSONObject(response);
            JSONArray json = jsonObj.getJSONArray("mData");
            for (int i = json.length() -1; i >= 0; i--) {
                Log.d("entp304", "OBJ " + (i + 1) + ": " + json.getJSONObject(i).toString());
                String title = json.getJSONObject(i).getString("jobTitle");
                int farmerId = json.getJSONObject(i).getInt("farmerId");
                int requestId = json.getJSONObject(i).getInt("requestId");
                double wage = json.getJSONObject(i).getDouble("hourlyRate");
//                String farmerName = json.getJSONObject(i).getString("farmName");
                int numHours = json.getJSONObject(i).getInt("numHoursPerWorker");
                int numWorkers = json.getJSONObject(i).getInt("numWorkers");

                mData.add(new farmerRequest(title, farmerId, wage, numWorkers, numHours, requestId, ""));
            }

        } catch (final JSONException e) {
            e.printStackTrace();
            return;
        }

        rv.setAdapter(adapter);
        rv.setLayoutManager(new LinearLayoutManager(this.getApplicationContext()));
    }


    protected StringRequest getResponse() {
        return (new StringRequest(Request.Method.GET, Variables.url + "requests",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("entp304", "VOLLEY: " + response);
                        populateList(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("entp304", "That Get didn't work!");
            }
        }));
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        MySingleton.getInstance(this).addToRequestQueue(getResponse());
        MySingleton.getInstance(this).addToRequestQueue(getFarmerResponse());
    }

}
