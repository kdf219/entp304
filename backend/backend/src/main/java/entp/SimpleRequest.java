package entp;

/**
 * FarmerRequest provides a format for clients to present title and message
 * strings to the server.
 * <p>
 * NB: since this will be created from JSON, all fields must be public, and we
 * do not need a constructor.
 */
public class SimpleRequest {
    public int farmerId;
    public int numWorkers;
    public int numHours;
    public double rate;
    public String jobTitle;

}
