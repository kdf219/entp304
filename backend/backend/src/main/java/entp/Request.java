package entp;

import java.sql.ResultSet;

class Request {

    int requestId;
    int farmerId;
    int numWorkers;
    int numHoursPerWorker;
    double hourlyRate;
    String jobTitle;
    String farmName;

    public Request(int requestId, int farmerId, int numWorkers, int numHoursPerWorker, double hourlyRate, String jobTitle, String farmName) {
        this.requestId = requestId;
        this.farmerId = farmerId;
        this.numWorkers = numWorkers;
        this.numHoursPerWorker = numHoursPerWorker;
        this.hourlyRate = hourlyRate;
        this.jobTitle = jobTitle;
        this.farmName = farmName;
    }

    public Request(ResultSet rs) {
        try {
            this.requestId = rs.getInt("requestId");
            this.farmerId = rs.getInt("farmerId");
            this.numWorkers = rs.getInt("numberWorkers");
            this.numHoursPerWorker = rs.getInt("numberHoursPerWorker");
            this.hourlyRate = rs.getDouble("hourlyRate");
            this.jobTitle = rs.getString("jobTitle");
            this.farmName = rs.getString("farmName");
        } catch (Exception e){

        }
    }
}