package entp;

// Import the Spark package, so that we can make use of the "get" function to
// create an HTTP GET route

import spark.Spark;
import spark.Spark.*;
import com.google.gson.*;
import spark.*;

import java.util.Map;

/**
 * For now, our app creates an HTTP server with only one route.
 * When an HTTP client connects to this server on the default SPARK port (4567),
 */
public class App {
    public static void main(String[] args) {
        Spark.port(getIntFromEnv("PORT", 4567));

        final Gson gson = new Gson();

        final Database database = Database.getDatabase(Database.getDatabaseUrl());

        // Set up the location for serving static files
        Spark.staticFileLocation("/web");

        final String acceptCrossOriginRequestsFrom = "*";
        final String acceptedCrossOriginRoutes = "GET,PUT,POST,DELETE,OPTIONS";
        final String supportedRequestHeaders = "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin";
        enableCORS(acceptCrossOriginRequestsFrom, acceptedCrossOriginRoutes, supportedRequestHeaders);

        // Set up a route for serving the main page
        Spark.get("/", (req, res) -> {
            res.redirect("/about.html");
            return "";
        });

        Spark.get("/about", (req, res) -> {
            res.redirect("/about.html");
            return "";
        });


        Spark.get("/profile", (req, res) -> {
            res.redirect("/profile.html");
            return "";
        });


        //get all farmers
        Spark.get("/farmers", (req, res) -> {
            res.status(200);
            res.type("application/json");
            return gson.toJson(new StructuredResponse("ok", null, database.getAllFarmers()));
        });

        //get farmer by id
        Spark.get("/farmers/:id", (req, res) -> {
            res.status(200);
            res.type("application/json");
            int id = Integer.parseInt(req.params("id"));
            System.out.println("Farmer id: " + id);
            return gson.toJson(new StructuredResponse("ok", null, database.getFarmer(id)));
        });

        //get all requests
        Spark.get("/requests", (req, res) -> {
            res.status(200);
            res.type("application/json");
            return gson.toJson(new StructuredResponse("ok", null, database.getAllRequests()));
        });

        //get request by id
        Spark.get("/requests/:id", (req, res) -> {
            res.status(200);
            res.type("application/json");
            int id = Integer.parseInt(req.params("id"));
            return gson.toJson(new StructuredResponse("ok", null, database.getRequest(id)));
        });

        //get all requests by a farmer id
        Spark.get("/requests/farmer/:id", (req, res) -> {
            res.status(200);
            res.type("application/json");
            int id = Integer.parseInt(req.params("id"));
            return gson.toJson(new StructuredResponse("ok", null, database.getRequestsByFarmer(id)));
        });

        Spark.post("/requests", (request1, res) -> {
            SimpleRequest req = gson.fromJson(request1.body(), SimpleRequest.class);

            res.status(200);
            res.type("application/json");
            boolean newId = database.addRequest(req.farmerId, req.numWorkers, req.numHours, req.rate, req.jobTitle);
            if (!newId) {
                return gson.toJson(new StructuredResponse("error", "error performing insertion", null));
            } else {
                return gson.toJson(new StructuredResponse("ok", "" + newId, null));
            }
        });

        Spark.post("/newFarmer", (request1, res) -> {
            FarmerRequest req = gson.fromJson(request1.body(), FarmerRequest.class);

            res.status(200);
            res.type("application/json");
            boolean newId = database.addFarmer(req.username, req.farm, req.contactName, req.email, req.phoneNumber);
            if (!newId) {
                return gson.toJson(new StructuredResponse("error", "error performing insertion", null));
            } else {
                return gson.toJson(new StructuredResponse("ok", "" + newId, null));
            }
        });

    }

    /**
     * Get an integer environment varible if it exists, and otherwise return the
     * default value.
     *
     * @envar The name of the environment variable to get.
     * @defaultVal The integer value to use as the default if envar isn't found
     * @returns The best answer we could come up with for a value for envar
     */
    static int getIntFromEnv(String envar, int defaultVal) {
        ProcessBuilder processBuilder = new ProcessBuilder();
        if (processBuilder.environment().get(envar) != null) {
            return Integer.parseInt(processBuilder.environment().get(envar));
        }
        return defaultVal;
    }

    // Enables CORS on requests. This method is an initialization method and should be called once.
    private static void enableCORS(final String origin, final String methods, final String headers) {

        Spark.options("/*", (request, response) -> {

            String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }

            String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) {
                response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }

            return "OK";
        });

        Spark.before((request, response) -> {
            response.header("Access-Control-Allow-Origin", origin);
            response.header("Access-Control-Request-Method", methods);
            response.header("Access-Control-Allow-Headers", headers);
            // Note: this may or may not be necessary in your particular application
            response.type("application/json");
        });
    }

}