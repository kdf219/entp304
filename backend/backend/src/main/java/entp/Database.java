package entp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.net.*;

import java.util.ArrayList;

class Database {
    private static String farmerTbl = "farmerUserTable";
    private static String requestTbl = "farmerRequestsTable";

    private Connection connection;

    private PreparedStatement selectAllFarmers;
    private PreparedStatement selectFarmer;
    private PreparedStatement selectAllRequests;
    private PreparedStatement selectRequest;
    private PreparedStatement selectRequestsByFarmer;

    private PreparedStatement newRequest;
    private PreparedStatement newFarmer;

    /**
     * The Database constructor is private: we only create Database objects
     * through the getDatabase() method.
     */
    private Database() {
    }

    /**
     * Get a fully-configured connection to the database
     *
     * @param ip   The IP address of the database server
     * @param port The port on the database server to which connection requests
     *             should be sent
     * @param user The user ID to use when connecting
     * @param pass The password to use when connecting
     * @return A Database object, or null if we cannot connect properly
     */
    static Database getDatabase(String url) {
        // Create an un-configured Database object
        Database db = new Database();
        // Give the Database object a connection, fail if we cannot get one
        try {
            Connection conn = getConnection(url);
            if (conn == null) {
                System.err.println("Error: DriverManager.getConnection() returned a null object");
                return null;
            }
            db.connection = conn;

        } catch (SQLException e) {
            System.err.println("Error: DriverManager.getConnection() threw a SQLException");
            e.printStackTrace();
            return null;
        } catch (URISyntaxException e) {
            System.err.println("Error: DriverManager.getConnection() threw a URISyntaxException");
            e.printStackTrace();
        }

        try {
            db.selectAllFarmers = db.connection.prepareStatement("SELECT * FROM " + farmerTbl);
            db.selectFarmer = db.connection.prepareStatement("SELECT * FROM " + farmerTbl + " WHERE userId = ?");
            db.selectAllRequests = db.connection.prepareStatement("SELECT * FROM " + requestTbl);
            db.selectRequest = db.connection.prepareStatement("SELECT * FROM " + requestTbl + " WHERE requestId = ?");
            db.selectRequestsByFarmer = db.connection.prepareStatement("SELECT * FROM " + requestTbl + " WHERE farmerId = ?");
            db.newRequest = db.connection.prepareStatement("INSERT INTO " + requestTbl + " VALUES (default,?,?,?,?,?)");
            db.newFarmer = db.connection.prepareStatement("INSERT INTO " + farmerTbl + " VALUES (default,?,?,?,?,?)");
        } catch (SQLException e) {
            return null;
        }

        return db;
    }


    private static Connection getConnection(String dbUrl) throws URISyntaxException, SQLException {
        return DriverManager.getConnection(dbUrl);
    }

    ArrayList<Farmer> getAllFarmers() {
        ArrayList<Farmer> res = new ArrayList<>();
        try {
            ResultSet rs = selectAllFarmers.executeQuery();
            while (rs.next()) {
                res.add(new Farmer(rs.getInt("userId"), rs.getString("username"), rs.getString("farm"),
                        rs.getString("contactName"), rs.getString("email"), rs.getString("phoneNumber")));
            }
            rs.close();
            return res;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    Farmer getFarmer(int id) {
        Farmer res = null;
        try {
            selectFarmer.setInt(1, id);
            ResultSet rs = selectFarmer.executeQuery();
            if (rs.next()) {
                res = new Farmer(rs.getInt("userId"), rs.getString("username"), rs.getString("farm"),
                        rs.getString("contactName"), rs.getString("email"), rs.getString("phoneNumber"));
            }
            return res;

        } catch (SQLException e) {
            e.printStackTrace();
            return res;
        }

    }

    ArrayList<Request> getAllRequests() {
        ArrayList<Request> res = new ArrayList<>();
        try {
            ResultSet rs = selectAllRequests.executeQuery();
            while (rs.next()) {
                res.add(new Request(rs));
            }
            rs.close();
            return res;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    Request getRequest(int id) {
        Request res = null;
        try {
            selectRequest.setInt(1, id);
            ResultSet rs = selectRequest.executeQuery();
            if (rs.next()) {
//                res = new Request(rs.getInt("requestId"), rs.getInt("farmerId"), rs.getInt("numberWorkers"),
//                        rs.getInt("numberHoursPerWorker"), rs.getInt("hourlyRate"), rs.getString("jobTitle"));

                res = new Request(rs);
            }
            return res;

        } catch (SQLException e) {
            e.printStackTrace();
            return res;
        }

    }

    Request getRequestsByFarmer(int id) {
        Request res = null;
        try {
            selectRequestsByFarmer.setInt(1, id);
            ResultSet rs = selectRequestsByFarmer.executeQuery();
            if (rs.next()) {
//                res = new Request(rs.getInt("requestId"), rs.getInt("farmerId"), rs.getInt("numberWorkers"),
//                        rs.getInt("numberHoursPerWorker"), rs.getInt("hourlyRate"), rs.getString("jobTitle"));

                res = new Request(rs);
            }
            return res;

        } catch (SQLException e) {
            e.printStackTrace();
            return res;
        }

    }

    boolean addRequest(int farmerId, int numWorkers, int numHours, double rate, String jobTitle) {
        try {
            newRequest.setInt(1, farmerId);
            newRequest.setInt(2, numWorkers);
            newRequest.setInt(3, numHours);
            newRequest.setDouble(4, rate);
            newRequest.setString(5, jobTitle);

            newRequest.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    boolean addFarmer(String username, String farm, String contactName, String email, String phoneNumber) {
        try {
            newFarmer.setString(1, username);
            newFarmer.setString(2, farm);
            newFarmer.setString(3, contactName);
            newFarmer.setString(4, email);
            newFarmer.setString(5, phoneNumber);

            newFarmer.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Close the current connection to the database, if one exists.
     * <p>
     * NB: The connection will always be null after this call, even if an
     * error occurred during the closing operation.
     *
     * @return True if the connection was cleanly closed, false otherwise
     */
    boolean disconnect() {
        if (connection == null) {
            System.err.println("Unable to close connection: connection was null");
            return false;
        }
        try {
            connection.close();
        } catch (SQLException e) {
            System.err.println("Error: connection.close() threw a SQLException");
            e.printStackTrace();
            connection = null;
            return false;
        }
        connection = null;
        return true;
    }

    static String getDatabaseUrl() {
        ProcessBuilder processBuilder = new ProcessBuilder();
        return processBuilder.environment().get("JDBC_DATABASE_URL");
    }

}