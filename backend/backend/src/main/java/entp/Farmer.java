package entp;

class Farmer {
    int uId;
    String username;
    String farmName;
    String contactName;
    String email;
    String phoneNumber;

    public Farmer(int uId, String username, String farmName, String contactName, String email, String phoneNumber) {
        this.uId = uId;
        this.username = username;
        this.farmName = farmName;
        this.contactName = contactName;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "Farmer{" +
                "uId=" + uId +
                ", username='" + username + '\'' +
                ", farmName='" + farmName + '\'' +
                ", contactName='" + contactName + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }
}