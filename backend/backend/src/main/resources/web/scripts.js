
function loadGrid(theme, nrows, ncols, grid) {
    $("#idlist").html(theme);
    $("#grid").empty();
    for (var i = 0; i < nrows; i++) {
        var newRow = $("<tr></tr>");
        for (var j = 0; j < ncols; j++) {
            var newCell = $("<td></td>");
            $(newCell).html('');
            $(newCell).append(grid.charAt((i * nrows) + j));
            $(newCell).attr("class", "unselected");
            $(newCell).attr("id", i + "_" + j);
            $(newRow).append(newCell);
        }
        $("#grid").append(newRow);
    }
}

// loadStatus reloads the leader board from the list of players
function loadStatus(players) {
    var usergrid = "";
    for (var i = 0; i < players.length; i++) {
        var player = players[i];
        var row = "<tr style='background-color:" + (player.winner ? "gold" : "white") + "'>" +
                "<td class='pname'>" + player.name + "</td>" +
                "<td class='pscore'>" + player.score + "</td>" +
                "</tr>";
        usergrid += row;
    }
    $("#userlist tbody").html(usergrid);
}
function updateGrid(newWords) {
    var wordArr = newWords.words;
    console.log(wordArr.length + " currently words found");
    for (var i = 0; i < wordArr.length; i++) {
        console.log(wordArr[i].text);
        var letterArr = wordArr[i].letters;
        for (var j = 0; j < letterArr.length; j++) {
            var id = letterArr[j].r + "_" + letterArr[j].c;
            $("#" + id).attr("class", "foundunsel");
        }
    }
}

function nullFcn(result) {
}

var HOST = "cse264.info:3000";
var SERVER = "http://" + HOST + "/";

// Utility method for encapsulating the jQuery Ajax Call
function doAjaxCall(method, cmd, params, fcn) {
    $.ajax(
            SERVER + cmd,
            {
                type: method,
                processData: true,
                data: params,
                dataType: "json",
                success: function (result) {
                    fcn(result);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Error: " + jqXHR.responseText);
                    alert("Error: " + textStatus);
                    alert("Error: " + errorThrown);
                }
            }
    );
}

var myid = 0;

function login(loginname) {
    if (loginname !== "") {
        doAjaxCall("GET", "wordsearch/login", {username: loginname},
                function (result) {
                    myid = result.id;
                    $("#lname").html(result.username);
                    getPuzzleFromServer();
                });
    } else {
        alert("Did not enter user name!");
    }


}
function getPuzzleFromServer() {
    doAjaxCall("GET", "wordsearch/puzzle", {id: myid},
            function (result) {
                loadGrid(result.theme,
                        result.nrows,
                        result.ncols,
                        result.grid);
            });
}

// Submit a set to the server
function submitSet() {
//    console.log(letterList.length !== 0);
//    console.log(letterList);
    if (letterList.length !== 0) {
        doAjaxCall("GET", "wordsearch/submit", {id: myid, letters: letterList}, nullFcn);
    } else {
        alert("Did not select any letters");
    }
    $(".selected").attr("class", "unselected");
    $(".foundsel").attr("class", "foundunsel");
    letterList.splice(0, letterList.length);
}


var letterList = new Array();

function selectLetter(tdelem) {
    var classType = $(tdelem).attr("class");
    var loc = $(tdelem).attr("id").split("_");
    var jloc = {r: parseInt(loc[0]), c: parseInt(loc[1])};
    if (classType === "unselected") {
        $(tdelem).attr("class", "selected");
        letterList.push(jloc);
    } else if (classType === "selected") {
        $(tdelem).attr("class", "unselected");
        var index = findIndex(jloc);
        letterList.splice(index, 1);
    } else if (classType === "foundunsel") {
        $(tdelem).attr("class", "foundsel");
        letterList.push(jloc);
    } else if (classType === "foundsel") {
        $(tdelem).attr("class", "foundunsel");
        var index = findIndex(jloc);
        letterList.splice(index, 1);
    } else {
        console.log("oops");
    }
}

function findIndex(jloc) {
    for (var i = 0; i < letterList.length; i++) {
        if (letterList[i].r === jloc.r) {
            if (letterList[i].c === jloc.c) {
                return i;
            }
        }
    }
    return -1;
}

// Attach an event handler to each button on the page
function attachEventHandlers() {
    $("#login").click(function () {
        login($("#loginname").val());
        $("#loginname").val("");
    });
    $("#grid").on("click", "td", function () {
        selectLetter(this);
    });
    $("#btnSet").click(function () {
        submitSet();
    });
}

// Run the setup code when the page finishes loading
$(() => {
    attachEventHandlers();
    var socket = io.connect(SERVER);

    // Update the card grid when a new card list arrives from the server
    socket.on('gridupdates', function (letters) {
        updateGrid(letters);
    });

    // Update the leader board when an updated player list arrives from the server
    socket.on('players', function (players) {
        loadStatus(players);
    });
});
